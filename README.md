Volt-Oven
=========
A minimalist, Lua-based game engine.

About
-----
A long time ago, a simple game engine was incepted and written in VB6 - Volt.
It couldn't do much, was 2D / sidescrolling, and barely counted as a game
(although I've seen _less_ passed as a game). This engine needed something...
**Scripting**.

And so began a long quest to design a game engine that easily maps onto a
scripting language that itself is easy to use, yet powerful and efficient enough
to _take control_ of the game engine and become its nervous system.

Over the years to come much R&D was put in - into engine designs, into OpenGL,
into audio programming and so forth, and scripting languages in general.

Fast forward to today: a variation of a WIP game engine happened to materialize
in a way suitable for the vision I had from those years ago. That engine I
called: godsource. Through that, Lua as the driver of an entire game engine came
to be possible. But that wasn't enough; a new design was needed since Lua does
not play nice with multi-threaded programs... and avoiding the issue has
become impossible...

And so the OVEN design was concieved, permitting Lua to exist isolated in its
own parallel process, without changing the Lua runtime one bit. Hence the name
**Volt-Oven**.

The design allows Lua to exert high level control over the various subsystems of
a game engine. Lua itself can be scripted to be a domain-specific language, and
the end result becomes a minimalist Lua-based game engine, with most of the mess
of parallel processing and resource management conveniently dealt with.

Motivation
----------
If you've ever tried to create a game, or any interactive multimedia, you would
be familiar with the effort and time it takes to get anything working to an
acceptable point. Now imagine you don't have the luxury of a high level editor,
or an editor capable of generating code for the behavior you need - what took
1 month suddenly takes 10.

This is a problem; some game designs / ideas simply do not fit in with the
sledgehammer one-size-fits-all off-the-shelf game engines. Not to name names in
particular, however Unreal offers an example: most Unreal-based games tend to
work off of a player weapon inventory - anything the player can use / equip, is
internally a weapon (generally the simplest way to achieve this but not the
only).

This doesn't make those game engines bad. Not at all, in fact often it's
_because_ of the optimizations that engine has to offer, that you're forced into
particular designs of game code. Call me crazy but game code should be agnostic
of the engine it is written in, just like any other program need not care about
operating system specifics for what one would consider an every-day task:
playing sounds, drawing models to the screen, and so on.

But the critiscism still stands: if you want to write game code a particular way
to some end / reason, it should be possible. And if you want to create a game on
your own, as far as game logic goes, you shouldn't need a team of people to do
it. That you do need so many people means you can't explain how your game works
in a straight-forward manner, and that's often simply not true...

The games I wanted to create once, did not agree with the designs I've seen
pushed by particular engines and/or derivative works (and their vehement
followers) at the time... My answer was to create an engine that can, even for
the craziest ideas I may have later. And the rest is history...