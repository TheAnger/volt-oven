VOLT_IO_ACQUIRE()
print(_VERSION,"test: counter")
VOLT_IO_RELEASE()

for x = 1, 10000000 do
	local l = os.clock ()
	if 0 == x % 1000000 then
		VOLT_IO_ACQUIRE()
		print("counter test: i =",x)
		VOLT_IO_RELEASE()
	end
end

