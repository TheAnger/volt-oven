#include "volt-core.hpp"

volt::sem _volt_io_sem( 1 );

void _volt_io_begin_exclusive (void)
{
  _volt_io_sem.wait();
  printf( "[%X] ", (unsigned int) pthread_self().p );
}
void _volt_io_end_exclusive (void)
{
  _volt_io_sem.post();
}

