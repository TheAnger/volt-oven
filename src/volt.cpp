//#define VOLT_CLEAN  /* omit debug messages */

#include "volt.hpp"

int main ()
{
  setvbuf( stdout, NULL, _IONBF, 0 );
  setvbuf( stderr, NULL, _IONBF, 0 );

  printf( "----------------------------------------------------\n" );
  VOLT_PRINT( "main() // start" );
  VOLT_PRINT( "waiting 5 seconds, then spawning 2 threads." );
  VOLT_PRINT( "after 5 more seconds both threads will be killed." );
  VOLT_PRINT( "5 seconds after that program ends." );
  Sleep( 5000 );
  VOLT_PRINT( "Creating & launching threads in sequence" );
  {
    volt::thread T1;
    VOLT_TEST( T1.spawn() );
    VOLT_TEST( T1.Lua_LoadFile( "lua/count.lua" ) );
    volt::thread T2;
    VOLT_TEST( T2.spawn() );
    VOLT_TEST( T2.Lua_LoadFile( "lua/count.lua" ) );
    Sleep( 5000 );
    VOLT_PRINT( "Killing threads" );
    T1.kill();
    T2.kill();
    Sleep( 4000 );
    VOLT_PRINT( "Exiting in 1 second" );
    Sleep( 1000 );
    VOLT_PRINT( "Exiting" );
  }

  VOLT_PRINT( "main() // end" );
  printf( "----------------------------------------------------\n" );
  pthread_exit( 0 );
  return 0;
}
