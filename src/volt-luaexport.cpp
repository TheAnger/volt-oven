#include "volt-luaexport.hpp"

namespace volt {
  namespace lua_export {

    /*
     * global io lock
     */

    static int _volt_global_io_acquire (lua_State * L)
    {
      _volt_io_begin_exclusive();
      printf( "Lua acquired global io\n" );
      return 0;
    }

    static int _volt_global_io_release (lua_State * L)
    {
      printf( "Lua released global io\n" );
      _volt_io_end_exclusive();
      return 0;
    }

    /*
     * VarPipe - inter-thread io streams
     */

    // sends value from current stack to target thread's inbox
    static int _volt_sendvalue (lua_State * L)
    {
      if ( !lua_islightuserdata( L, lua_upvalueindex( 1 ) ) )
        return lua_pushboolean( L, 0 ), 1;
      int n = lua_gettop( L );
      if ( n != 2 )
        return lua_pushboolean( L, 0 ), 1;
      unsigned long targetID;
      {
        int validlong = 0;
        targetID = lua_tounsignedx( L, 1, &validlong );
        if ( !validlong )
          return lua_pushboolean( L, 0 ), 1;
      }
      return lua_pushboolean( L,
        ((thread*) lua_touserdata( L, lua_upvalueindex( 1 ) ))->msgSend( 2, targetID ) ), 1;
    }

    // reads value from current thread's inbox onto the current stack
    static int _volt_readvalue (lua_State * L)
    {
      if ( !lua_islightuserdata( L, lua_upvalueindex( 1 ) ) )
        return lua_pushboolean( L, 0 ), 1;
      return lua_pushboolean( L,
        ((thread*) lua_touserdata( L, lua_upvalueindex( 1 ) ))->msgRead() ), 1;
    }

    /*
     * openlibs
     */

    void openlibs (lua_State * L, thread * LuaThread)
    {
      // standard libraries
      luaL_openlibs( L );

      // global io lock
      lua_pushcclosure( L, &_volt_global_io_acquire, 0 ), lua_setglobal( L, "VOLT_IO_ACQUIRE" );
      lua_pushcclosure( L, &_volt_global_io_release, 0 ), lua_setglobal( L, "VOLT_IO_RELEASE" );

      // VarPipe
      lua_pushlightuserdata( L, (void*) LuaThread );
      lua_pushvalue( L, -1 );
      lua_pushcclosure( L, &_volt_sendvalue, 1 ), lua_setglobal( L, "VOLT_THREAD_SENDVALUE" );
      lua_pushcclosure( L, &_volt_readvalue, 1 ), lua_setglobal( L, "VOLT_THREAD_READVALUE" );
    }

  }
}

