#include "volt-lua.hpp"

namespace volt {
  namespace lualib {

    static int toBuffer (lua_State *, const void *b, size_t l, void *d)
    {
      luaL_addlstring( (luaL_Buffer*) d, (const char*) b, l );
      return 0;
    }
    static bool read_lua_scalar (bool & is_scalar, lua_State * L, int idx, LuaValue * value)
    {
      switch ( int luaType = lua_type( L, idx ) )
      {
        case LUA_TNONE:
        case LUA_TNIL:
        {
          value->type = luaType;
          return (is_scalar = true);
        }

        case LUA_TBOOLEAN:
        {
          value->type = luaType;
          value->B = lua_toboolean( L, idx );
          return (is_scalar = true);
        }

        case LUA_TUSERDATA:
        case LUA_TLIGHTUSERDATA:
        {
          value->type = LUA_TLIGHTUSERDATA;
          value->P = lua_touserdata( L, idx );
          return (is_scalar = true);
        }

        case LUA_TNUMBER:
        {
          value->N = lua_tonumberx( L, idx, 0 );
          value->type = luaType;
          return (is_scalar = true);
        }

        case LUA_TFUNCTION:
        {
          // functions stored as string of it's bytecode dump
          // proceeds into LUA_STRING, rewriting the original call
          luaL_Buffer Accum;
          lua_pushvalue( L, idx );
          lua_dump( L, &toBuffer, (luaL_buffinit( L, &Accum ), (void*) &Accum) );
          luaL_pushresult( &Accum );
          lua_remove( L, -2 );
          idx = -1;
        }
        // no break

        case LUA_TSTRING:
        {
          size_t str_len;
          const char * src = lua_tolstring( L, idx, &str_len );
          value->type = luaType;
          value->S.c = new char[ str_len ];
          value->S.l = str_len;
          memcpy( value->S.c, src, str_len );
          return (is_scalar = true);
        }

        default:
        return !(is_scalar = false);
      }
    }
    static bool read_lua_table (lua_State * L, int idx, LuaValue * value)
    {
      int resetidx = lua_gettop( L );
      value->type = LUA_TTABLE;
      value->T = new LuaTable;

      std::stack <LuaTableImportIterator> tblstack;
      tblstack.push( { value->T } );

      int vstk, vstk_sz = 0;

      lua_pushvalue( L, idx );     // stack: tbl
      lua_createtable( L, 4, 0 );  // stack: tbl vstk
      vstk = lua_gettop( L );

      lua_insert( L, --vstk );     // stack: vstk tbl

      lua_pushnil( L );            // stack: vstk tbl nil
      lua_pushnil( L );            // stack: vstk tbl nil nil

      lua_rawseti( L, vstk, ++vstk_sz );
      lua_rawseti( L, vstk, ++vstk_sz );
      lua_rawseti( L, vstk, ++vstk_sz );
      // stack: vstk
      // vstk:  nil nil tbl

      bool captured;
      while ( !tblstack.empty() ) {
        LuaTableImportIterator &top = tblstack.top();
        auto tblstack_sz = tblstack.size();

        lua_settop( L, vstk );

        if ( tblstack.top().state <= 2 ) {
          // only necessary if there are values to extract
          lua_rawgeti( L, vstk, vstk_sz-- );
          lua_rawgeti( L, vstk, vstk_sz-- );
          lua_rawgeti( L, vstk, vstk_sz-- );
          // stack: vstk tbl prev-key prev-val
          // vstk:  -3 slots from top
        }

        while ( tblstack_sz == tblstack.size() )
          switch ( top.state )
          {
            case 0: // next pair
              // stack: vstk tbl prev-key prev-val
              if ( 0 == lua_next( L, vstk + 1 ) ) {
                // end of tbl
                lua_settop( L, vstk );
                // stack: vstk
                tblstack.pop();
                break;
              } else
                // stack: vstk tbl key val
                top.state = 1;
              // no break
            case 1: // eval key
              // stack: vstk tbl key val
              if ( !read_lua_scalar( captured, L, vstk + 2, &top.key ) )
                // rollback
                return reset_variant( value ), lua_settop( L, resetidx ), false;
              else if ( captured )
                top.state = 2;
              else {
                top.key.type = LUA_TTABLE;
                top.key.T = new LuaTable;

                top.state = 2;
                tblstack.push( { top.key.T } );

                lua_rawseti( L, vstk, ++vstk_sz );
                lua_rawseti( L, vstk, ++vstk_sz );
                lua_rawseti( L, vstk, ++vstk_sz );
                // stack: vstk
                // vstk:  val key tbl

                break;
              }
              // no break
            case 2: // eval val
              {
                // stack: vstk tbl key val
                if ( lua_type( L, vstk + 3 ) == LUA_TTABLE ) {
                  top.value.type = LUA_TTABLE;
                  top.value.T = new LuaTable;

                  ++top.state;
                  tblstack.push( { top.value.T } );

                  lua_rawseti( L, vstk, ++vstk_sz );
                  lua_rawseti( L, vstk, ++vstk_sz );
                  lua_rawseti( L, vstk, ++vstk_sz );
                  // stack: vstk
                  // vstk:  val key tbl

                  break;
                }

                if ( !read_lua_scalar( captured, L, vstk + 3, &top.value ) ) {
                  // rollback
                  reset_variant( value );
                  lua_settop( L, resetidx );
                  return false;
                }

                ++top.state;
              }
              // no break
            case 3: // copy key-val pair to table
              {
                // stack: vstk tbl key val
                (*top.tbl)[ top.key ] = top.value;
                top.state = 0; // reset iterator state for tbl
              }
          }
      }
      lua_settop( L, resetidx );
      return true;
    }

    bool read_lua_variant (lua_State * L, int idx, LuaValue * value)
    {
      bool is_scalar;

      if ( !read_lua_scalar( is_scalar, L, idx, value ) ) {
        // rollback
        reset_variant( value );
        return false;
      }

      if ( is_scalar )
        return true;

      if ( !read_lua_table( L, idx, value ) ) {
        // rollback
        reset_variant( value );
        return false;
      }

      return true;
    }

    bool write_lua_variant (lua_State * L, const LuaValue * value)
    {
      int resetidx = lua_gettop( L );
      switch ( value->type )
      {
        case LUA_TNONE:
          return true;
        case LUA_TNIL:
          return lua_pushnil( L ), true;
        case LUA_TBOOLEAN:
          return lua_pushboolean( L, value->B ), true;
        case LUA_TLIGHTUSERDATA:
          return lua_pushlightuserdata( L, value->P ), true;
        case LUA_TNUMBER:
          return lua_pushnumber( L, value->N ), true;
        case LUA_TSTRING:
          return lua_pushlstring( L, value->S.c, value->S.l ), true;
        case LUA_TFUNCTION:
          if ( luaL_loadbufferx( L, value->S.c, value->S.l, "push_variant<FUNCTION>", 0 ) )
            return lua_settop( L, resetidx ), false;
          else
            return true;
        case LUA_TTABLE:
          if ( value->T->empty() ) {
            lua_newtable( L );
            return true;
          } else {
            std::stack <LuaTableExportIterator> tblstack;
            tblstack.push( { value->T } );
            lua_newtable( L );
            lua_newtable( L );
            int q = lua_gettop( L );
            int res = q - 1, tbl = q + 1;
            int q_sz = 0;
            lua_pushvalue( L, res );
            lua_rawseti( L, q, ++q_sz );
            // stack: res=table q=table
            // q: res
            while ( !tblstack.empty() ) {
              auto tblstack_sz = tblstack.size();
              lua_settop( L, q );
              // stack: res=table q=table
              LuaTableExportIterator &top = tblstack.top();
              while ( tblstack_sz != tblstack.size() )
                switch ( top.state )
                {
                  case 0:
                    // stack: res=table q=table
                    // q: tbl
                    if ( top.pos->second.type == LUA_TTABLE ) {
                      lua_newtable( L );
                      if ( top.pos->second.T->empty() ) {
                        lua_rawseti( L, q, ++q_sz );
                        // stack: res=table q=table
                        // q: tbl value={}
                        top.state = 1;
                      } else {
                        lua_pushvalue( L, -1 );
                        lua_rawseti( L, q, ++q_sz );
                        lua_rawseti( L, q, ++q_sz );
                        // stack: res=table q=table
                        // q: tbl v={} v
                        top.state = 1;
                        tblstack.push( { top.pos->second.T } );
                        break;
                      }
                    } else if ( write_lua_variant( L, &top.pos->second ) ) {
                      // stack: res=table q=table value
                      // q: tbl
                      lua_rawseti( L, q, ++q_sz );
                      // stack: res=table q=table
                      // q: tbl value
                      top.state = 1;
                    } else
                      // rollback
                      return lua_settop( L, resetidx ), false;
                    // no break
                  case 1:
                    // stack: res=table q=table
                    // q: tbl value
                    if ( top.pos->first.type == LUA_TTABLE ) {
                      lua_newtable( L );
                      if ( top.pos->first.T->empty() ) {
                        lua_rawseti( L, q, ++q_sz );
                        // stack: res=table q=table
                        // q: tbl value key={}
                        top.state = 2;
                      } else {
                        lua_pushvalue( L, -1 );
                        lua_rawseti( L, q, ++q_sz );
                        lua_rawseti( L, q, ++q_sz );
                        // stack: res=table q=table
                        // q: tbl key k={} k
                        top.state = 2;
                        tblstack.push( { top.pos->first.T } );
                        break;
                      }
                    } else if ( write_lua_variant( L, &top.pos->first ) ) {
                      // stack: res=table q=table key
                      // q: tbl value
                      lua_rawseti( L, q, ++q_sz );
                      // stack: res=table q=table
                      // q: tbl value key
                      top.state = 2;
                    } else
                      // rollback
                      return lua_settop( L, resetidx ), false;
                    // no break
                  case 2:
                    // stack: res=table q=table
                    // q: tbl value key
                    {
                      lua_rawgeti( L, q, q_sz - 2 );
                      lua_rawgeti( L, q, q_sz-- );
                      lua_rawgeti( L, q, q_sz-- );
                      // stack: res=table q=table tbl key value
                      // q: tbl
                      lua_rawset( L, tbl );
                      // stack: res=table q=table tbl
                      // q: tbl
                      lua_settop( L, q );
                      // stack: res=table q=table
                      if ( top.pos != top.end )
                        tblstack.pop();
                      else {
                        top.pos++;
                        top.state = 0;
                      }
                    }
                }
            }
            return true;
          }
        default:
          return false;
      }
    }

    void reset_variant (LuaValue * value)
    {
      if ( value->type == LUA_TFUNCTION || value->type == LUA_TSTRING )
        delete[] value->S.c;

      if ( value->type == LUA_TTABLE ) {
        LuaValue t;
        for ( auto kv : *value->T )
          reset_variant( (t = kv.second, &t) ), reset_variant( (t = kv.first, &t) );
        delete value->T;
      }

      value->type = -1;
    }
  }
}
