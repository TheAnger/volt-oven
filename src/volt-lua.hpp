#ifndef VOLT_LUA_HPP_
#define VOLT_LUA_HPP_

#include "volt-core.hpp"
#include <deque>
#include <queue>
#include <vector>
#include <map>
#include <stack>

using namespace std;

namespace volt {
  namespace lualib {

    struct LuaValue;

    typedef std::map <LuaValue, LuaValue> LuaTable;

    typedef std::queue <LuaValue> LuaQueue;

    struct LuaValue
    {
        int type;
        union
        {
            int B;
            void *P;
            LuaTable *T;
            double N;
            struct
            {
                char *c;
                size_t l;
            } S;
        };

        explicit inline LuaValue (void * v)
          : type( LUA_TLIGHTUSERDATA ), P( v )
        {
        }

        explicit inline LuaValue (LuaTable * v)
          : type( LUA_TTABLE ), T( v )
        {
        }

        explicit inline LuaValue (bool v)
          : type( LUA_TBOOLEAN ), B( (int) v )
        {
        }

        inline LuaValue (double v)
          : type( LUA_TNUMBER ), N( (double) v )
        {
        }

        inline LuaValue (void)
          : type( LUA_TNONE )
        {
        }

        inline const bool operator == (const LuaValue &rhs) const
        {
          const LuaValue &lhs = *this;
          if ( lhs.type != rhs.type )
            return false;

          if ( &lhs == &rhs )
            return true;

          switch ( lhs.type )
          {
            case LUA_TNONE:
            case LUA_TNIL:
              return true;

            case LUA_TBOOLEAN:
              return (lhs.B == rhs.B);

            case LUA_TNUMBER:
              return (lhs.N == rhs.N);

            case LUA_TLIGHTUSERDATA:
              return (lhs.P == rhs.P);

            case LUA_TTABLE:
              return (lhs.T == rhs.T);

            case LUA_TUSERDATA:
            case LUA_TSTRING:
            case LUA_TFUNCTION:
              return ((lhs.S.l == rhs.S.l)
                && (lhs.S.l == 0 || lhs.S.c == rhs.S.c || 0 == memcmp( lhs.S.c, rhs.S.c, lhs.S.l )));

            default:
              return false;
          }
        }
        inline const bool operator != (const LuaValue &rhs) const
        {
          const LuaValue &lhs = *this;
          return !(lhs == rhs);
        }

        inline const bool operator < (const LuaValue &rhs) const
        {
          const LuaValue &lhs = *this;
          if ( lhs.type != rhs.type )
            return (lhs.type < rhs.type);

          if ( &lhs == &rhs )
            return false;

          switch ( lhs.type )
          {
            case LUA_TNONE:
            case LUA_TNIL:
              return false;

            case LUA_TBOOLEAN:
              return (lhs.B < rhs.B);

            case LUA_TNUMBER:
              return (lhs.N < rhs.N);

            case LUA_TLIGHTUSERDATA:
            case LUA_TTABLE:
              return (lhs.P < rhs.P);

            case LUA_TUSERDATA:
            case LUA_TSTRING:
            case LUA_TFUNCTION:
              if ( lhs.S.l == 0 || lhs.S.c == rhs.S.c )
                return false;
              if ( lhs.S.l != rhs.S.l )
                return (lhs.S.l < rhs.S.l);
              return (memcmp( lhs.S.c, rhs.S.c, lhs.S.l ) < 0);

            default:
              return false;
          }
        }
        inline const bool operator > (const LuaValue &rhs) const
        {
          const LuaValue &lhs = *this;
          return (rhs < lhs);
        }
        inline const bool operator <= (const LuaValue &rhs) const
        {
          const LuaValue &lhs = *this;
          return !(rhs < lhs);
        }
        inline const bool operator >= (const LuaValue &rhs) const
        {
          const LuaValue &lhs = *this;
          return !(lhs < rhs);
        }

    };

    struct LuaTableImportIterator
    {
        LuaTable * tbl = nullptr;
        LuaValue key, value;
        int state = 0;
        LuaTableImportIterator (void) = default;
        LuaTableImportIterator (LuaTable * t)
          : tbl( t )
        {
        }
    };
    struct LuaTableExportIterator
    {
        LuaTable * tbl = nullptr;
        LuaTable::iterator pos, end;
        int state = 0;
        LuaTableExportIterator (void) = default;
        LuaTableExportIterator (LuaTable * t)
          : tbl( t )
        {
          pos = tbl->begin();
          end = tbl->end();
        }
    };

    bool read_lua_variant (lua_State * L, int idx, LuaValue * value);

    bool write_lua_variant (lua_State * L, const LuaValue * value);

    void reset_variant (LuaValue * value);

  }
}

#endif /* VOLT_LUA_HPP_ */
