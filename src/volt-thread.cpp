#include "volt-thread.hpp"
#include "volt-luaexport.hpp"
#include <list>

static volt::cowpat::tlock ThreadListLock;
static std::list <volt::thread*> Threads = { };

namespace volt {

  thread::thread (void)
    : LuaGate( 0 )
  {
    VOLT_PRINT( "thread.thread() // start" );
    VOLT_ASSERTZ( pthread_attr_init( &PThreadAttributes ) );
    VOLT_ASSERTZ( pthread_attr_setdetachstate( &PThreadAttributes, PTHREAD_CREATE_JOINABLE ) );
    if ( ThreadListLock.wlock(), Threads.size() ) {
      std::list <unsigned long> ids = { };
      for ( auto t : Threads )
        ids.push_back( t->LuaTID );
      ids.sort();
      for ( auto i : ids )
        if ( i != LuaTID )
          break;
        else
          LuaTID++;
    }
    Threads.push_back( this );
    ThreadListLock.unlock();
    VOLT_PRINT( "LuaTID = %lu", LuaTID );
    VOLT_PRINT( "thread.thread() // end" );
  }

  thread::~thread (void)
  {
    VOLT_PRINT( "thread.~thread() // start" );
    kill(), wait_exit();
    auto Token = Lock.write_token();
    pthread_attr_destroy( &PThreadAttributes );
    ThreadListLock.wlock(), Threads.remove( this ), ThreadListLock.unlock();
    VOLT_PRINT( "thread.~thread() // end" );
  }

  bool thread::spawn (void)
  {
    VOLT_PRINT( "thread.spawn() // start" );
    auto Token = Lock.write_token();
    int pthread_error = 0;
    if ( State >= Spawned ) {
      VOLT_PRINT( "thread::spawn() - already spawned" );
      return false;
    }
    if ( (pthread_error = pthread_create( &PThread, &PThreadAttributes, thread::main, (void*) this )) ) {
      VOLT_PRINT( "thread::spawn() - pthread_error %d", pthread_error );
      return false;
    }
    State = Spawned;
    VOLT_PRINT( "thread.spawn() // end" );
    return true;
  }

  void * thread::main (void * arg)
  {
    VOLT_PRINT( "thread::main() // start" );
    {
      thread * Thread = (thread*) arg;
      auto Token = Thread->Lock.token();
      VOLT_ASSERT( Token.wlock() );
      if ( VOLT_TEST( (Thread->State == Spawned && (Thread->LuaState = luaL_newstate())) ) ) {
        lua_export::openlibs( Thread->LuaState, Thread );
        Thread->State = Ready;
        while ( Thread->State == Ready ) {
          Token.unlock(), Thread->LuaGate.wait();
          VOLT_ASSERT( Token.wlock() );
          if ( VOLT_TEST( Thread->State != Ready ) )
            break;
          if ( VOLT_TEST( !lua_gettop( Thread->LuaState ) ) )
            break;
          if ( VOLT_TEST( !lua_isfunction( Thread->LuaState, -1 ) ) )
            break;
          Thread->State = Busy;
          if ( VOLT_TEST( lua_pcallk( Thread->LuaState, 0, LUA_MULTRET, 0, 0, 0 ) ) ) {
            VOLT_PRINT( lua_tolstring( Thread->LuaState, -1, 0 ) );
            break;
          }
          if ( Thread->State == Busy )
            Thread->State = Ready;
        } // while
        lua_close( Thread->LuaState );
      }
      Thread->State = Exiting;
    }
    VOLT_PRINT( "thread::main() // end" );
    pthread_exit( NULL );
    return NULL;
  }

  void thread::kill (void)
  {
    VOLT_PRINT( "thread.kill() // start" );
    auto Token = Lock.write_token();
    if ( State < Killed ) {
      State = Killed;
      LuaGate.post();
    }
    VOLT_PRINT( "thread.kill() // end" );
  }

  void thread::wait_exit (void)
  {
    VOLT_PRINT( "thread.wait_exit() // start" );
    void * result;
    auto Token = Lock.write_token();
    if ( !Joined ) {
      Joined = true;
      Token.unlock(), pthread_join( PThread, &result );
      VOLT_PRINT( "thread.wait_exit() // end" );
      return;
    }
    while ( Token.rlock() && State != Exiting )
      Token.unlock(), sched_yield();
    VOLT_PRINT( "thread.wait_exit() // end" );
  }

  bool thread::wait_ready (void)
  {
    VOLT_PRINT( "thread.wait_ready() // start" );
    auto Token = Lock.token();
    if ( !Token.rlock() || State < Spawned )
      return false;
    while ( State != Ready )
      if ( Token.unlock(), sched_yield(), !Token.rlock() || State >= Killed )
        return false;
    VOLT_PRINT( "thread.wait_ready() // end" );
    return true;
  }

  bool thread::Lua_LoadFile (const char * filename)
  {
    auto Token = Lock.token();
    if ( !Token.wlock() || State < Spawned || State >= Killed )
      return false;
    while ( State != Ready )
      if ( Token.unlock(), sched_yield(), !Token.wlock() || State >= Killed )
        return false;
    if ( LuaGate.try_wait() )
      return LuaGate.post(), false;
    if ( luaL_loadfilex( LuaState, filename, 0 ) )
      return false;
    return LuaGate.post(), true;
  }

  bool thread::msgSend (int idx, unsigned long destLuaTID)
  {
    lualib::LuaValue tempval;
    auto Token = ThreadListLock.token();
    if ( !Token.rlock() )
      return false;
    for ( auto dest : Threads )
      if ( dest->LuaTID == destLuaTID ) {
        if ( dest->msgGate.wait(), !lualib::read_lua_variant( LuaState, idx, &tempval ) )
          return dest->msgGate.post(), false;
        return dest->msgInbox.push( tempval ), dest->msgGate.post(), true;
      }
    return false;
  }

  bool thread::msgRead (void)
  {
    lualib::LuaValue tempval;
    if ( msgGate.wait(), msgInbox.empty() )
      return msgGate.post(), false;
    tempval = msgInbox.front();
    return msgInbox.pop(), msgGate.post(), lualib::write_lua_variant( LuaState, &tempval );
  }
}
