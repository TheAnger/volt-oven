#ifndef VOLT_THREAD_HPP_
#define VOLT_THREAD_HPP_

#include "volt-core.hpp"
#include "volt-lua.hpp"

namespace volt {

  class thread
  {
      /*
       * thread class - implements an Lua stack within a pthread.
       */

    public:

      enum threadState
      {
        Empty,
        Spawned,
        Ready,
        Busy,
        Killed,
        Exiting
      };

    private:

      volatile bool Joined = false;
      volatile threadState State = Empty;

      sem LuaGate, msgGate;

      cowpat::tlock Lock;

      lua_State * LuaState = nullptr;

      lualib::LuaQueue msgInbox;

      pthread_t PThread;
      pthread_attr_t PThreadAttributes;

      static void * main (void * arg);

      unsigned long LuaTID = 1;

    public:

      thread (void);
      ~thread (void);

      bool spawn (void);
      void kill (void);

      bool wait_ready (void);
      void wait_exit (void);

      bool Lua_LoadFile (const char * filename);

      bool msgSend (int idx, unsigned long destLuaTID);
      bool msgRead (void);
  };
}

#endif /* VOLT_THREAD_HPP_ */
