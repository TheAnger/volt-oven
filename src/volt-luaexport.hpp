#ifndef VOLT_LUAEXPORT_HPP_
#define VOLT_LUAEXPORT_HPP_

#include "volt-core.hpp"
#include "volt-lua.hpp"
#include "volt-thread.hpp"

namespace volt {
  namespace lua_export {

    void openlibs (lua_State * L, thread * LuaThread);

  }
}

#endif /* VOLT_LUAEXPORT_HPP_ */
