#ifndef VOLT_CORE_HPP_
#define VOLT_CORE_HPP_

#include <pthread.h>
#include <stdio.h>
#include <semaphore.h>
#include <cerrno>
#include <exception>
#include <new>
#include <list>

#include <lua.hpp>

void _volt_io_begin_exclusive (void);
void _volt_io_end_exclusive (void);

#define VOLT_ASSERTZ(x)  { \
  if (x) { \
    _volt_io_begin_exclusive(); \
    printf( "%s:%d: failed on assert(false == (\n\t%s\n))\n", __FILE__, __LINE__, #x ); \
    exit( 1 ); \
  } \
}
#define VOLT_ASSERT(x)  { \
  if (!(x)) { \
    _volt_io_begin_exclusive(); \
    printf( "%s:%d: failed on assert(\n\t%s\n)\n", __FILE__, __LINE__, #x ); \
    exit( 1 ); \
  } \
}

#define VOLT_PRINT(...)  { \
  _volt_io_begin_exclusive(); \
  printf( "%s:%d: ", __FILE__, __LINE__ ); \
  printf( __VA_ARGS__ ); \
  printf( "\n" ); \
  _volt_io_end_exclusive(); \
}

#define VOLT_TEST(x)  ((x) \
  ? (_volt_io_begin_exclusive(), \
    printf( "%s:%d: test( %s ) == True\n", __FILE__, __LINE__, #x ), \
    _volt_io_end_exclusive(), \
    true) \
  : (_volt_io_begin_exclusive(), \
    printf( "%s:%d: test( %s ) == False\n", __FILE__, __LINE__, #x ), \
    _volt_io_end_exclusive(), \
    false) \
)

#ifdef VOLT_CLEAN

#undef VOLT_PRINT
#undef VOLT_TEST

#define VOLT_PRINT(...)  {}
#define VOLT_TEST(x)  (x)

#endif

namespace volt {

  class sem
  {
      /*
       * sem - implements semaphores
       */
      sem_t Semaphore;
    public:
      inline sem (unsigned int initial = 1)
      {
        VOLT_ASSERTZ( sem_init( &Semaphore, 0, initial ) );
      }
      inline ~sem (void)
      {
        VOLT_ASSERTZ( sem_destroy( &Semaphore ) );
      }
      inline void wait (void)
      {
        VOLT_ASSERTZ( sem_wait( &Semaphore ) );
      }
      inline bool try_wait ()
      {
        int _sem_trywait = sem_trywait( &Semaphore );
        VOLT_ASSERT( _sem_trywait == 0 || _sem_trywait == EAGAIN || _sem_trywait == -1 );
        return _sem_trywait == 0;
      }
      inline void post (void)
      {
        VOLT_ASSERTZ( sem_post( &Semaphore ) );
      }
  };

  class rwlock
  {
      /*
       * rwlock - implements pthread-brand rwlock
       */
      pthread_rwlock_t RWLock;
    public:
      inline rwlock (void)
      {
        VOLT_ASSERTZ( pthread_rwlock_init( &RWLock, 0 ) );
      }
      inline ~rwlock (void)
      {
        VOLT_ASSERTZ( pthread_rwlock_destroy( &RWLock ) );
      }
      inline void rlock (void)
      {
        VOLT_ASSERTZ( pthread_rwlock_rdlock( &RWLock ) );
      }
      inline void wlock (void)
      {
        VOLT_ASSERTZ( pthread_rwlock_wrlock( &RWLock ) );
      }
      inline void unlock (void)
      {
        VOLT_ASSERTZ( pthread_rwlock_unlock( &RWLock ) );
      }
      inline bool try_rlock (void)
      {
        int _pthread_rwlock_rdlock = pthread_rwlock_tryrdlock( &RWLock );
        VOLT_ASSERT( _pthread_rwlock_rdlock == 0 || _pthread_rwlock_rdlock == EBUSY );
        return _pthread_rwlock_rdlock == 0;
      }
      inline bool try_wlock (void)
      {
        int _pthread_rwlock_wrlock = pthread_rwlock_trywrlock( &RWLock );
        VOLT_ASSERT( _pthread_rwlock_wrlock == 0 || _pthread_rwlock_wrlock == EBUSY );
        return _pthread_rwlock_wrlock == 0;
      }
  };

  namespace cowpat {

    class tsem
    {
        /*
         * tsem - implements termination-safe semaphores
         */
        volatile bool Go = true;
        volatile unsigned int Que = 0;
        sem Ex, Sem;
      public:
        inline tsem (unsigned int initial = 1)
          : Sem( initial )
        {
        }
        inline bool try_wait (void)
        {
          if ( Ex.wait(), Go )
            return Ex.post(), Sem.try_wait();
          return Ex.post(), false;
        }
        inline bool wait (void)
        {
          if ( Ex.wait(), !Go )
            return Ex.post(), false;
          if ( Sem.try_wait() )
            return Ex.post(), true;
          if ( ++Que, Ex.post(), Sem.wait(), Ex.wait(), Go )
            return --Que, Ex.post(), true;
          return --Que, Ex.post(), false;
        }
        inline bool post (void)
        {
          if ( Ex.wait(), Go )
            return Sem.post(), Ex.post(), true;
          return Ex.post(), false;
        }
        inline bool lock_wait (void)
        {
          if ( Ex.wait(), !Go )
            return Ex.post(), false;
          Go = false;
          while ( Que ) {
            if ( Sem.try_wait() )
              Sem.post();
            Ex.post(), sched_yield(), Ex.wait();
          }
          return true;
        }
        inline void unlock_wait (void)
        {
          VOLT_ASSERT( Go == false );
          Go = true;
          Ex.post();
        }
        inline ~tsem (void)
        {
          Ex.wait();
          Go = false;
          while ( Que ) {
            if ( Sem.try_wait() )
              Sem.post();
            Ex.post(), sched_yield(), Ex.wait();
          }
          Ex.post();
        }
    };

    class tlock
    {
        /*
         * termination-safe rwlock.
         * blocking wait calls no longer guarantee a lock,
         * instead return a bool indicating success.
         * the new failure reason for blocking waits is
         * exclusive write lock, which must be released
         * in a special way (or the lock enters a state
         * where no lock can be acquired again).
         */
        volatile unsigned int WriteQueue = 0, ReadQueue = 0, Readers = 0;
        volatile bool Unlocked = true, Go = true;
        sem Ex;
        rwlock Lock;
      public:
        inline tlock (void)
        {
          VOLT_PRINT( "tlock::tlock() created" );
        }
        inline bool rlock (void)
        {
          if ( Ex.wait(), !Go )
            return Ex.post(), false;
          if ( Unlocked && Lock.try_rlock() )
            return ++Readers, Ex.post(), true;
          if ( ++ReadQueue, Ex.post(), Lock.rlock(), Ex.wait(), Go )
            return ++Readers, --ReadQueue, Ex.post(), true;
          return --ReadQueue, Lock.unlock(), Ex.post(), false;
        }
        inline bool wlock (void)
        {
          if ( Ex.wait(), !Go )
            return Ex.post(), false;
          if ( Unlocked && Lock.try_wlock() ) {
            Unlocked = false;
            return Ex.post(), true;
          }
          if ( ++WriteQueue, Ex.post(), Lock.wlock(), Ex.wait(), Go ) {
            Unlocked = false;
            return --WriteQueue, Ex.post(), true;
          }
          return --WriteQueue, Lock.unlock(), Ex.post(), false;
        }
        inline bool try_rlock (void)
        {
          if ( Ex.wait(), !(Go && Unlocked && Lock.try_rlock()) )
            return Ex.post(), false;
          return ++Readers, Ex.post(), true;
        }
        inline bool try_wlock (void)
        {
          if ( Ex.wait(), !(Go && Unlocked && Lock.try_wlock()) )
            return Ex.post(), false;
          Unlocked = false;
          return Ex.post(), true;
        }
        inline void unlock (void)
        {
          if ( Ex.wait(), Unlocked )
            --Readers;
          Unlocked = true;
          Lock.unlock(), Ex.post();
        }
        inline bool exclusive_wlock (void)
        {
          /*
           * force contenders to fail.
           * wait for existing contenders to fail.
           */
          if ( !this->wlock() )
            return false;
          Ex.wait();
          VOLT_PRINT( "tlock.exclusive_wlock() acquired" );
          Go = false;
          Unlocked = true;
          while ( VOLT_TEST( (WriteQueue || ReadQueue || Readers) ) )
            Lock.unlock(), Ex.post(), sched_yield(), Lock.wlock(), Ex.wait();
          return Ex.post(), true;
        }
        inline void exclusive_unlock (void)
        {
          Ex.wait();
          VOLT_PRINT( "tlock.exclusive_wlock() released" );
          Unlocked = Go = true;
          Lock.unlock(), Ex.post();
        }
        inline ~tlock (void)
        {
          /*
           * must clear off all other contenders first.
           * basic concept: exclusive_wlock() followed by regular unlock()
           */
          VOLT_PRINT( "tlock.~tlock() released" );
          // repeat exclusive lock attempts, and in between
          // wait for read-lock to be available to wait for
          // write lock to be relinquished.
          while ( !exclusive_wlock() )
            Lock.rlock(), Lock.unlock();
          unlock();
        }
        class access_token;
        inline access_token token (void)
        {
          return {this,0};
        }
        inline access_token read_token (void)
        {
          return {this,1};
        }
        inline access_token write_token (void)
        {
          return {this,2};
        }
        class access_token
        {
            friend class tlock;
            tlock *Lock;
            bool Latched, Elevated;
            inline access_token (tlock * L, int mode)
              : Lock( L ), Latched( mode > 0 ), Elevated( mode > 1 )
            {
              if ( Elevated )
                Latched = Lock->wlock();
              else if ( Latched )
                Latched = Lock->rlock();
            }
          public:
//            inline explicit operator bool (void)
//            {
//              return Latched;
//            }
            inline bool rlock (void)
            {
              if ( Latched ) {
                if ( !Elevated )
                  return true;
                Lock->unlock();
                Elevated = false;
              }
              return (Latched = Lock->rlock());
            }
            inline bool wlock (void)
            {
              if ( Latched ) {
                if ( Elevated )
                  return true;
                Lock->unlock();
                Elevated = true;
              }
              return (Latched = Lock->wlock());
            }
            inline bool try_rlock (void)
            {
              if ( Latched ) {
                if ( !Elevated )
                  return true;
                Lock->unlock();
                Elevated = false;
              }
              return (Latched = Lock->try_rlock());
            }
            inline bool try_wlock (void)
            {
              if ( Latched ) {
                if ( Elevated )
                  return true;
                Lock->unlock();
                Elevated = true;
              }
              return (Latched = Lock->try_wlock());
            }
            inline void unlock (void)
            {
              if ( Latched )
                return;
              Lock->unlock();
              Latched = false;
            }
            inline ~access_token (void)
            {
              unlock();
            }
        };
    };
  }
}

#endif /* VOLT_CORE_HPP_ */
